<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '2}*NFpZqs5 *4sC&9xo}<IsJtv|_,hRp#<AT(~$+DFxB$4nl)o %=SCrub7BqVC#');
define('SECURE_AUTH_KEY',  'F5ZG*0Z&S)[p~TId,}R)i3D$MIR?OhlE4&f#Kip2:@DwubQ,Y]&eS@ MVtQK%1nD');
define('LOGGED_IN_KEY',    'Y.4npR&O^J|f+PQ47AraQfU[~R^Wiy^vTc|sUlq);LBDsB@_!L{J3pnB:t #`PEI');
define('NONCE_KEY',        'JDZq7j/au$+qEouKbELE-`@n9RJw2}T+71&kH)JHi+o@2GT$FvrRkh<-dm#uH(aW');
define('AUTH_SALT',        'HaZU;N`)o}ol3./Ah/ET&1~{CXV{VH$<eQ<.l3iBDW{yOef?A$@pcVWs[j[Q{(YT');
define('SECURE_AUTH_SALT', '80v IlU-mtFl`)$RuO1V8Q]2Iunc|$V=xF<iOk.R+-/&YpI0R%e+UmBjqynR6m,~');
define('LOGGED_IN_SALT',   'RxP)<0+CV4eFJT7$7}iakPOiL^YgyVW[O&@qc{z*}SaCjRh{VXXV;NbqyFPU*L=r');
define('NONCE_SALT',       'kS&5g?n*=**&Sf%7%^wGaV?I3r6ag.y6_$CXz$w;;XDfLA&8G2$**`j[ <^ajS&,');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

define( 'WP_ALLOW_MULTISITE', true );

define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', true);
define('DOMAIN_CURRENT_SITE', 'wordpress.loc');
define('PATH_CURRENT_SITE', '/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);



/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

define('SUNRISE', 'on');
/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

