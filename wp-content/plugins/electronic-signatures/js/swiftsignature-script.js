jQuery(document).ready(function ($) {
    /* swiftcloud upgrade notice */
    /* go live Tuesday August 7th */
    var now = new Date();
    var m = now.getMonth(); // m = 0-11
    var d = now.getDate();  // d = 1-31
    if (m === 7 && d === 7) {
        jQuery('.ss_alert_notice').show();
    }
});
//Email validation
function ValidateEmail(mail) {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
        return (true);
    }
    return (false);
}