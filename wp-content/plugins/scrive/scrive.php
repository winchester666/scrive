<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              scrive
 * @since             1.0.0
 * @package           Scrive
 *
 * @wordpress-plugin
 * Plugin Name:       scrive
 * Plugin URI:        scrive.com
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            scrive
 * Author URI:        scrive
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       scrive
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'PLUGIN_NAME_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-scrive-activator.php
 */
function activate_scrive() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-scrive-activator.php';
	Scrive_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-scrive-deactivator.php
 */
function deactivate_scrive() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-scrive-deactivator.php';
	Scrive_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_scrive' );
register_deactivation_hook( __FILE__, 'deactivate_scrive' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-scrive.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_scrive() {

	$plugin = new Scrive();
	$plugin->run();

}
run_scrive();


# Default configuration
const BASE_URL = 'https://scrive.com/api';
const API_VERSION = '/v2';
// const CONSUMER_KEY='51c6b2456a341dd2_1011';
// const CONSUMER_SECRET='755ef5205e7fc1a9';
// const OAUTH_TOKEN='0e9134bfd163e3c9_970';
// const OAUTH_TOKEN_SECRET='4eae5fdaedced3bf';


function getOAuth(){

	$body['email'] = 'eugene.b@halo-lab.com';
	$body['password'] = '1q2w3e4r';

	$args = array('method' => 'POST','httpversion' => '2.0', 'body' => $body);
	$url = BASE_URL.API_VERSION.'/getpersonaltoken';

	$curl = new WP_Http_Curl();
	$auth = $curl->request( $url, $args );

	return $auth;
}

// hook for authorized users
add_action( 'wp_ajax_getList', 'getListScrive' );
// hook for unauthorized users
add_action( 'wp_ajax_nopriv_getList', 'getListScrive' );
function getListScrive(){

	$auth_key = getOAuth();
	$oauth_keys = json_decode($auth_key['body']);

	$headers["Authorization"] = 'oauth_realm="Scrive",oauth_signature_method="PLAINTEXT", oauth_consumer_key="'.$oauth_keys->apitoken.'", oauth_token="'.$oauth_keys->accesstoken.'",  oauth_signature="'.$oauth_keys->apisecret.'&'.$oauth_keys->accesssecret.'"';
	
	$args = array('method' => 'GET','httpversion' => '2.0','headers'=> $headers);


	$url = BASE_URL.API_VERSION.'/documents/list';

	$curl = new WP_Http_Curl();
	$getList = $curl->request($url, $args);
	// $scriveList = json_decode($getList['body']);
	wp_send_json($getList['body']);
	die;
}


// hook for authorized users
add_action( 'wp_ajax_downloadScrive', 'downloadScrive' );
// hook for unauthorized users
add_action( 'wp_ajax_nopriv_downloadScrive', 'downloadScrive' );
function downloadScrive(){
	$id = $_POST['id'];

	$auth_key = getOAuth();
	$oauth_keys = json_decode($auth_key['body']);

	$headers["Authorization"] = 'oauth_realm="Scrive",oauth_signature_method="PLAINTEXT", oauth_consumer_key="'.$oauth_keys->apitoken.'", oauth_token="'.$oauth_keys->accesstoken.'",  oauth_signature="'.$oauth_keys->apisecret.'&'.$oauth_keys->accesssecret.'"';
	
	$args = array('method' => 'GET','httpversion' => '2.0','headers'=> $headers, 'timeout' => 0);
	
	$url = BASE_URL.API_VERSION.'/documents/'.$id.'/files/main/';

	$curl = new WP_Http_Curl();
	$downloadFile = $curl->request($url, $args);
	// var_dump($downloadFile['body']);

	// $scriveList = json_decode($downloadFile['body']);
	wp_send_json($downloadFile);
	die;
}

// hook for authorized users
add_action( 'wp_ajax_QRScrive', 'QRScrive' );
// hook for unauthorized users
add_action( 'wp_ajax_nopriv_QRScrive', 'QRScrive' );
function QRScrive(){
	$id = $_POST['id'];
	$signatory_id = $_POST['signatory_id'];

	$auth_key = getOAuth();
	$oauth_keys = json_decode($auth_key['body']);

	$headers["Authorization"] = 'oauth_realm="Scrive",oauth_signature_method="PLAINTEXT", oauth_consumer_key="'.$oauth_keys->apitoken.'", oauth_token="'.$oauth_keys->accesstoken.'",  oauth_signature="'.$oauth_keys->apisecret.'&'.$oauth_keys->accesssecret.'"';
	
	$args = array('method' => 'GET','httpversion' => '2.0','headers'=> $headers, 'timeout' => 0);
	
	$url = BASE_URL.API_VERSION.'/documents/'.$id.'/'.$signatory_id.'/getqrcode/';

	$curl = new WP_Http_Curl();
	$QRcode = $curl->request($url, $args);
	$base64 = base64_encode($QRcode['body']);
	// return $base64;
	// $scriveList = json_decode($downloadFile['body']);
	wp_send_json($base64);
	die;
}
