(function( $ ) {
	'use strict';

	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */
	 $("#get_list").click(function(event) {
			$.ajax({
				url: '<?php echo admin_url("admin-ajax.php") ?>',
				type: 'POST',
				data: 'action=getList',
				dataType: "json"
			})
			.done(function(response) {
				console.log("success");
				console.log(response);
				var list = jQuery.parseJSON(response);
				console.log(list['documents']);
				$.each(list['documents'], function(index, val) {
					var link = "";
					var qrcode = "";
					var template = "";
					if(val["shareable_link"] != null){
						link = '(<a href="https://scrive.com'+val["shareable_link"]+'" target="_blank">Link</a>)';
					}
					if(val["status"] === "pending"){
						qrcode = '<span class="get_qr">< QR code ></span>';
					}
					if(val["is_template"] === true){
						template = ' (Template)';
					}
					$('#scrive_table tbody').append('<tr class="scrive_list_item" data-id="' + val["id"] + '" data-signatory_id="' + val["viewer"]["signatory_id"] + '"><td><span class="get_title">' + val["title"]+template + '</span></td><td>'+val["status"]+'</td><td>'+link+'</td><td>'+qrcode+'</td></tr>')

				});
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		});

		$('#scrive_table').on('click','.get_title', function(event) {
			event.preventDefault();
			$.ajax({
				url: '<?php echo admin_url("admin-ajax.php") ?>',
				type: 'POST',
				data: {
					action: 'downloadScrive',
					id: $(this).parent().data('id')
				}
				// dataType: "json"
			})
			.done(function(response) {
				console.log("success");
				console.log(response['body']);
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		});
		$('#scrive_table').on('click','.get_qr', function(event) {
			event.preventDefault();

			var click_btn = $(this);
			$.ajax({
				url: '<?php echo admin_url("admin-ajax.php") ?>',
				type: 'POST',
				data: {
					action: 'QRScrive',
					id: $(this).closest('.scrive_list_item').data('id'),
					signatory_id: $(this).closest('.scrive_list_item').data('signatory_id')
				}
				// dataType: "json"
			})
			.done(function(response) {
				console.log("success");

				click_btn.parent().append('<img src="data:image/png;base64,'+ response +'" />');
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		});
})( jQuery );

