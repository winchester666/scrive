<?php

/**
 * Fired during plugin deactivation
 *
 * @link       scrive
 * @since      1.0.0
 *
 * @package    Scrive
 * @subpackage Scrive/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Scrive
 * @subpackage Scrive/includes
 * @author     scrive <scrive@gmail.com>
 */
class Scrive_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
