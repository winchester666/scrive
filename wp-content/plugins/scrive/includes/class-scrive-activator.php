<?php

/**
 * Fired during plugin activation
 *
 * @link       scrive
 * @since      1.0.0
 *
 * @package    Scrive
 * @subpackage Scrive/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Scrive
 * @subpackage Scrive/includes
 * @author     scrive <scrive@gmail.com>
 */
class Scrive_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
