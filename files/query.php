<!DOCTYPE html>
<html>
	<head>
		<title>Space Manager Query Test</title>
	</head>
	
	<body>
	<table>
		<thead>
			<tr>
				<th>Address</th>
				<th>Site</th>
				<th>Phone</th>
				<th>Email</th>
				<th>KeypadZone</th>
				<th>TimeZone</th>
				<th>Status</th>
			</tr>
		</thead>
		<tbody>
		<?php
		include 'connect.php'; //make connection to database
			
			$customerId = 'XXXX'; //The customer ID to query
			
			$sql = "call WCustomerDetails('" . $customerId . "')"; //call a database procedure
			$result = sasql_query ( $connect, "$sql" );
				
			//return some data	
			while ( $row = sasql_fetch_array ( $result ) ) 
			{
				echo"
				<tr>
					<td>" . $row['Address'] . "</td>
					<td>" . $row['Site'] . "</td>
					<td>" . $row['Phone'] . "</td>
					<td>" . $row['Email'] . "</td>
					<td>" . $row['KeypadZone'] . "</td>
					<td>" . $row['TimeZone'] . "</td>
					<td>" . $row['Status'] . "</td>
				</tr>
					";
			}
		
		
		?>
		</tbody>
	</table>
</body>